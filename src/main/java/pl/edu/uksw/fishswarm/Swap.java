package pl.edu.uksw.fishswarm;

/**
 * Created by aniarudi on 02.02.16.
 */
public class Swap<T> {
    T spot;
    T replacer;

    public Swap(T spot, T replacer) {
        this.spot = spot;
        this.replacer = replacer;
    }

    public T getSpot() {
        return spot;
    }

    public T getReplacer() {
        return replacer;
    }

    @Override
    public String toString() {
        return ""+spot+" -> "+replacer;
    }
}
