package pl.edu.uksw.fishswarm;

import pl.edu.uksw.fishswarm.graph.Graph;
import pl.edu.uksw.fishswarm.graph.Node;

import java.util.*;

/**
 * Created by aniarudi on 01.02.16.
 */
public class NMI {
    Graph pseudograph;
    RybkaState2ListSet converter;
    List<Swap<Integer>> swapList;

    public List<Swap<Integer>> getSwapList() {
        return swapList;
    }

    public NMI(Graph pseudograph) {
        this.pseudograph = pseudograph;
        this.converter = new RybkaState2ListSet(pseudograph);
    }


    public double calculate(Rybka rybka1, Rybka rybka2) {
        swapList = new ArrayList<>();
        List<Set<Node>> rybka1state = converter.convert(rybka1);
        List<Set<Node>> rybka2state = converter.convert(rybka2);
        int Ca = rybka1state.size();
        int Cb = rybka2state.size();
        int max = Math.max(Ca, Cb);
        int [ ] [ ] Nij = new int [Cb] [Ca] ;
        int N = 0;
    //System.out.println(Cb+" "+ Ca);

        int[] sumCa = new int [Ca];
        int[] sumCb = new int [Cb];

        for(int i = 0; i< Ca; i++) {
            for (int j = 0; j< Cb; j++) {
                Set<Node> intersection = new HashSet<Node>(rybka1state.get(i));
                intersection.retainAll(rybka2state.get(j));
                Nij[j][i] = intersection.size();
                N += Nij[j][i];
                sumCb[j] += Nij[j][i];
                sumCa[i] += Nij[j][i];
            }
        }

        //debug(Nij);
        //System.out.println();

        // diagonalizacja
       /* for(int i=0; i < Ca; i++ ) {
            int searchingMax = i;
            for(int j=i; j < Cb; j++ ) {
                if(Nij[j][i] >  Nij[searchingMax][i]) {
                    searchingMax = j;
                }
            }
            if(searchingMax != i ) {
                int tmpRow[] = Nij[i];
                Nij[i] = Nij[searchingMax];
                Nij[searchingMax] = tmpRow;

                int i1 = sumCb[i];
                sumCb[i] = sumCb[searchingMax];
                sumCb[searchingMax] = i1;
                swapList.add(new Swap<>(i, searchingMax));
            }
        }
*/
        //debug(Nij);
        //System.out.println();
        //debug(new int[][]{sumCa, sumCb});
        //calculate final result
        double suma = 0., sumaCA = 0., sumaCB = 0.;

        for(int i = 0; i < Ca; i++) {
            for(int j=0; j < Cb; j++ ) {
                if ( Nij[j][i] != 0) {
                    suma += Nij[j][i] * (Math.log(Nij[j][i]) +Math.log( N) -Math.log(sumCa[i]) - Math.log(sumCb[j]) );
                    //System.out.println("" + suma + " " + Nij[j][i] + " Math.log( " + Nij[j][i] + "*" + N + "/" + sumCa[i] + "*" + sumCb[j]);
                    //System.out.println(-2 * Nij[j][i] * Math.log(Nij[j][i] * N / sumCa[i] * sumCb[j]));
                }
            }
        }
        suma *= -2.;
        for(int i = 0; i < Ca; i++) {
            sumaCA += sumCa[i] * (Math.log(sumCa[i]) -Math.log(N));
        //System.out.println(""+sumaCA+" "+sumCa[i]+" "+N);
        }
        for(int j = 0; j < Cb; j++) {
                sumaCB += sumCb[j] * (Math.log(sumCb[j]) -Math.log(N));
        //System.out.println(""+sumaCB+" "+sumCb[j]+" "+N);
        }
        //System.out.println(""+suma+" "+sumaCA+" "+sumaCB);
        suma = suma / (sumaCA + sumaCB);
        return suma ;
    }

    private void debug(int[][] nij) {
        for(int i = 0; i < nij.length; i++) {
            System.out.println(Arrays.toString(nij[i]));
        }
    }
}
