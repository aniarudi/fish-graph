package pl.edu.uksw.fishswarm;

import com.mxgraph.layout.mxCircleLayout;
import com.mxgraph.layout.mxIGraphLayout;
import com.mxgraph.swing.mxGraphComponent;
import org.jgraph.JGraph;
import org.jgrapht.ext.JGraphModelAdapter;
import org.jgrapht.ext.JGraphXAdapter;
import pl.edu.uksw.fishswarm.graph.Edge;
import pl.edu.uksw.fishswarm.graph.GmlParser;
import pl.edu.uksw.fishswarm.graph.Graph;
import pl.edu.uksw.fishswarm.graph.Node;

import javax.swing.*;
import java.awt.event.*;

/**
 * Hello world!
 *
 */
public class AppVis
{
    public static void main( String[] args )
    {

        System.out.println("test");
        Graph pseudograph = GmlParser.readFromFile("resources/test3.gml");
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //frame.setSize(1024, 768);
        frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
        JGraphXAdapter<Node, Edge> graph =
                new JGraphXAdapter<Node, Edge>(pseudograph);
        mxIGraphLayout layout = new mxCircleLayout(graph);

        layout.execute(graph.getDefaultParent());
        final mxGraphComponent graphComponent = new mxGraphComponent(graph);
        graphComponent.setConnectable(false);
        frame.getContentPane().add(graphComponent);
        frame.setVisible(true);

        graphComponent.addMouseWheelListener(new MouseWheelListener() {
            @Override
            public void mouseWheelMoved(MouseWheelEvent e) {
                if( (e.getModifiersEx() & InputEvent.CTRL_DOWN_MASK) != 0 ) {
                    if (e.getWheelRotation() < 0) {
                        graphComponent.zoomIn();
                    } else {
                        graphComponent.zoomOut();
                    }
                } else {
                    e.consume();
                }
            }
        });

    }
}
