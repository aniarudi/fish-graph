/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.uksw.fishswarm;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author rudi
 */
public class Plikopisanie {
    FileWriter fw;
    
    public Plikopisanie(String nazwaPliku) {
       try {
       fw = new FileWriter(nazwaPliku);
          } catch (IOException e) { 
        e.printStackTrace();
     }
    }
    
    public void dopisz(Object o){
              
        try {
            fw.write(""+o+'\n');
            } catch (IOException e) {
            e.printStackTrace();
        }
        
    }
    
    public void zamknij(){
        try{
            fw.close();
        } catch(IOException e) {
            e.printStackTrace();
        }
    }
    
    
}
