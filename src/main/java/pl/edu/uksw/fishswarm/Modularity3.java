/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
*/

package pl.edu.uksw.fishswarm;

import pl.edu.uksw.fishswarm.graph.Edge;
import pl.edu.uksw.fishswarm.graph.Graph;
import pl.edu.uksw.fishswarm.graph.Node;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by aniarudi on 04.02.16.
 */
public class Modularity3  extends Modularity{
   
    
    public Modularity3(Graph pseudograph) {
        this.pseudograph = pseudograph;
        this.converter = new RybkaState2ListSet(pseudograph);
    }

    public double calculate(List<Set<Node>> communities) {
        int size = communities.size();
        double[][] e = new double[size][size];
        double e2 = 0;
        double tre = 0;
        double allCount = pseudograph.getEdgeCount();
        for(int i=0; i<size;i++ ) {
            Set<Node> nodesi = communities.get(i);
            Set<Node> nodesj = communities.get(i);
                for (Node nodei : nodesi) {
                    for (Node nodej : nodesj) {
                        Edge edge = pseudograph.getEdge(nodei, nodej);
                        if (edge != null) {
                            tre += (edge.getWeight() / (2.0 * allCount));
                        }
                    }
                }
        }
        for(int i=0; i<size;i++ ) {
            Set<Node> nodesi = communities.get(i);
            Set<Node> nodesj = communities.get(i);
            for (Node nodei : nodesi) {
                for (Node nodej : nodesj) {
                    if(!nodei.equals(nodej)) {
                        e2 += ((double)pseudograph.degreeOf(nodej) / (2.0 * allCount)) *
                                ((double)pseudograph.degreeOf(nodei) / (2.0 * allCount));
                    }
                }
            }
        }


        return tre-e2;
    }
}
