package pl.edu.uksw.fishswarm;

import pl.edu.uksw.fishswarm.graph.Edge;
import pl.edu.uksw.fishswarm.graph.Graph;
import pl.edu.uksw.fishswarm.graph.Node;

import java.util.List;
import java.util.Set;

/**
 * Created by aniarudi on 03.02.16.
 */
public class Modularity1 extends Modularity{
   

    public List<Swap<Integer>> getSwapList() {
        return swapList;
    }

    public Modularity1(Graph pseudograph) {
        this.pseudograph = pseudograph;
        this.converter = new RybkaState2ListSet(pseudograph);
    }

    public double calculate(List<Set<Node>> communities) {

        double modularity = 0.0;
        double subsum = 0.0;
        double totalStrength = 0.0;
        for (Node n : pseudograph.vertexSet()) {
            totalStrength += n.getStrength();
        }
       
        //System.out.println("TS: " + totalStrength);

        for (Set<Node> community: communities) {
            //System.out.println(">>>>");
            double internalStrength = 0.0;
            double moduleStrength = 0.0;
            //System.out.println("IS: " + internalStrength);
            //System.out.println("MS: " + moduleStrength);
            for (Node n1 : community) {
                moduleStrength += n1.getStrength();
                for (Node n2 : community) {
                    //System.out.println(n1.id + "<>" + n2.id + "edges.count: " + this.getAllEdges(n1, n2).size());
                    for (Edge e : pseudograph.getAllEdges(n1, n2)) {
                         //System.out.println("e.wieght: " + e.getWeight());
                        internalStrength += e.getWeight();
                    }
                }
                
            }
            //System.out.println("IS: " + internalStrength);
            //System.out.println("MS: " + moduleStrength);
            //System.out.println("Subsum: " + subsum);
            subsum = internalStrength / (totalStrength);
            //System.out.println("Subsum: " + subsum);
            subsum -= (moduleStrength / totalStrength) * (moduleStrength / totalStrength);
            //System.out.println("Subsum: " + subsum);
            modularity += subsum;
        }
        return modularity;
    }
}
