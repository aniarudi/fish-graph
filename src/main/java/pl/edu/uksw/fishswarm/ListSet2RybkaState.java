package pl.edu.uksw.fishswarm;


import pl.edu.uksw.fishswarm.graph.Graph;
import pl.edu.uksw.fishswarm.graph.Node;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * Created by aniarudi on 31.01.16.
 */
public class ListSet2RybkaState {
    private final Graph graph;

    public ListSet2RybkaState(Graph graph) {
        this.graph = graph;
    }
    public Rybka convert(List<Set<Node>> communities) {
        int min = 0;
        int max = graph.getVertexCount();
        System.out.println(min+" "+max);
        System.out.println(graph.vertexSet());
        int[] fishState = new int[max-min];

        for ( Set<Node> society: communities) {
            Iterator<Node> iterator = society.iterator();
            Node first = iterator.next();
            Node previous = first;

            while(iterator.hasNext()) {
                Node next = iterator.next();
                fishState[previous.getId()-min] = next.getId();
                previous = next;
            }
            fishState[previous.getId()-min] = first.getId();

        }
        return new Rybka(fishState, min);
    }

    }
