package pl.edu.uksw.fishswarm;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.xy.DefaultXYDataset;
import org.jfree.data.xy.XYDataset;

import javax.swing.*;
import java.awt.*;

public class DotsChart extends JFrame {

    private static final long serialVersionUID = 1L;
    JFreeChart chart = null;
    DefaultXYDataset dataset = null;

    public DotsChart(String applicationTitle, String chartTitle, double[]  series) {
        super(applicationTitle);
        double maxval = 10;
        double[][] doubles = new double[2][series.length];
        for(int itr= 0; itr < series.length; itr++) {
            doubles[0][itr] = itr;
            doubles[1][itr] = series[itr];
        }
        // This will create the dataset
        dataset = createDataset( doubles);

        // based on the dataset we create the chart
        chart = createChart(dataset, chartTitle);
        // we put the chart into a panel
        ChartPanel chartPanel = new ChartPanel(chart);
        // default size
        chartPanel.setPreferredSize(new java.awt.Dimension(1024, 768));

        // add it to our application
        setContentPane(chartPanel);
        XYPlot plot = chart.getXYPlot();

        XYItemRenderer renderer = new StandardXYItemRenderer(StandardXYItemRenderer.LINES);
        plot.setRenderer(renderer);

        NumberAxis domain = (NumberAxis) plot.getDomainAxis();

        //domain.setRange(Application.CHART_CENTER-maxval, Application.CHART_CENTER+maxval);
        //domain.setTickUnit(new NumberTickUnit(1));
        //domain.setVerticalTickLabels(true);
        //NumberAxis range = (NumberAxis) plot.getRangeAxis();
        //range.setRange(Application.CHART_CENTER-maxval, Application.CHART_CENTER+maxval);
        //range.setTickUnit(new NumberTickUnit(1));


        plot.getRendererForDataset(plot.getDataset(0)).setSeriesPaint(0, Color.darkGray);

        plot.getRendererForDataset(plot.getDataset(0)).setSeriesPaint(1, new Color(50,205,50));
        plot.getRendererForDataset(plot.getDataset(0)).setSeriesPaint(2, Color.red);
        plot.getRendererForDataset(plot.getDataset(0)).setSeriesPaint(3, Color.blue);
    }


    /**
     * Creates a sample dataset
     */

    private DefaultXYDataset createDataset(double[][] series) {
        DefaultXYDataset result = new DefaultXYDataset();
        result.addSeries("label",series);
        return result;

    }

    public void addSeries(double[] series, String seriesLabel) {
        double[][] doubles = new double[2][series.length];
        for(int itr= 0; itr < series.length; itr++) {
            doubles[0][itr] = itr;
            doubles[1][itr] = series[itr];
        }
        dataset.addSeries( seriesLabel, doubles);
    }

    /**
     * Creates a chart
     */

    private JFreeChart createChart(XYDataset dataset, String title) {

        JFreeChart chart = ChartFactory.createXYLineChart(title,          // chart title
                "Values", "",
                dataset,                // data
                PlotOrientation.VERTICAL,                   // include legend
                false,
                false,
                false);

        XYPlot plot = (XYPlot) chart.getPlot();

        return chart;

    }
}