package pl.edu.uksw.fishswarm;

import java.util.Arrays;

/**
 * Created by aniarudi on 31.01.16.
 */
public class Rybka {

    private int[] state;
    private int firstVertex = 0;
    private Double modularity;


    public Rybka(int[] state, int firstVertex) {
        this.state = state;
        this.firstVertex = firstVertex;
    }

    public Rybka(int firstVertex, int vertexCount) {
        this.firstVertex = firstVertex;
        this.state = new int[vertexCount-firstVertex];
    }

    public int[] getState() {
        return state;
    }

    public int getState(int vertex) {
        return state[vertex-firstVertex];
    }

    public void setState(int vertex, int offset){
        state[vertex-firstVertex] = offset;
    }
    public int getFirstVertex() {
        return firstVertex;
    }

    @Override
    public String toString() {
        return "Rybka{" +
                "state=" + Arrays.toString(state) +
                " first index: "+firstVertex+
                '}';
    }

    public void setModularity(Double modularity) {
        this.modularity = modularity;
    }

    public Double getModularity() {
        return modularity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Rybka rybka = (Rybka) o;

        if (firstVertex != rybka.firstVertex) return false;
        if (!Arrays.equals(state, rybka.state)) return false;
        return modularity != null ? modularity.equals(rybka.modularity) : rybka.modularity == null;

    }

    @Override
    public int hashCode() {
        int result = Arrays.hashCode(state);
        result = 31 * result + firstVertex;
        result = 31 * result + (modularity != null ? modularity.hashCode() : 0);
        return result;
    }
}
