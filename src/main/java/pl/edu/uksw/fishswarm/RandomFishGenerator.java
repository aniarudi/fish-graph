package pl.edu.uksw.fishswarm;


import pl.edu.uksw.fishswarm.graph.Graph;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by aniarudi on 31.01.16.
 */
public class RandomFishGenerator {
    private int fishLength;
    private int firstVertexIndex;
    private Random random;
    private List<Integer> fullList;
    public RandomFishGenerator(int fishLength, int firstVertexIndex) {
        this.fishLength = fishLength;
        this.firstVertexIndex = firstVertexIndex;
        init();
    }

    public RandomFishGenerator(Graph graph) {
        this.fishLength = graph.getVertexCount();;
        this.firstVertexIndex = 0;
        
        init();
    }

   
    private void init() {
        this.random = new Random();
        fullList = new ArrayList<>();
        for(int i=0; i < fishLength; i++) {
            fullList.add(i,i);
        }
    }

    public Rybka randomFish(Graph graph) {
        List<Integer> tmp ;
        int[] fishState = new int[fishLength];
        for(int i=0; i < fishLength; i++) {
            tmp = new ArrayList<>(graph.getAdjListOfVertex(i));
            Integer indeks = tmp.get(random.nextInt(tmp.size()));
            
            fishState[this.firstVertexIndex+i] = indeks;
        }
        return new Rybka(fishState, this.firstVertexIndex);
    }

    public int[] randomSteps(int steps) {
        List<Integer> tmp = new ArrayList<>(fullList);
        int[] fishSteps = new int[steps];
        for(int i=0; i < steps; i++) {
            Integer indeks = tmp.get(random.nextInt(tmp.size()));
            tmp.remove(indeks);
            fishSteps[this.firstVertexIndex+i] = indeks;
        }
        return fishSteps;
    }
}
