package pl.edu.uksw.fishswarm;

import pl.edu.uksw.fishswarm.graph.Edge;
import pl.edu.uksw.fishswarm.graph.Graph;
import pl.edu.uksw.fishswarm.graph.Node;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by aniarudi on 04.02.16.
 */
public class Modularity2 {
    Graph pseudograph;
    RybkaState2ListSet converter;
    public Modularity2(Graph pseudograph) {
        this.pseudograph = pseudograph;
        this.converter = new RybkaState2ListSet(pseudograph);
    }

    public double calculate(List<Set<Node>> communities) {
        int size = communities.size();
        double[][] e = new double[size][size];
        double e2 = 0;
        double tre = 0;
        double allCount = pseudograph.getEdgeCount();
        for(int i=0; i<size;i++ ) {
            Set<Node> nodesi = communities.get(i);
            for (int j=i; j<size;j++) {
                Set<Node> nodesj = communities.get(j);
                Set<Edge> edges= new HashSet<>();
                for (Node nodei : nodesi) {
                    for (Node nodej : nodesj) {
                        Edge edge = pseudograph.getEdge(nodei, nodej);
                        if( edge != null) {
                            edges.add(edge);
                        }
                    }
                }
                e[i][j] = edges.size()/allCount;
                e[j][i] = e[i][j];
                if( i == j) {
                    tre += e[i][j];
                } else {
                    e2 += e[i][j] * e[i][j];
                }
            }
        }


        return tre-e2;
    }
}
