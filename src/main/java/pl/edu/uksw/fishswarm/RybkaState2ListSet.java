package pl.edu.uksw.fishswarm;

import pl.edu.uksw.fishswarm.graph.Graph;
import pl.edu.uksw.fishswarm.graph.Node;

import java.util.*;

/**
 * Created by aniarudi on 31.01.16.
 */
public class RybkaState2ListSet {
    private Graph graph;
    public RybkaState2ListSet(Graph graph) {
        this.graph = graph;
    }

    public List<Set<Node>> convert(Rybka rybka) {
        int[] state = Arrays.copyOf(rybka.getState(), rybka.getState().length);
        List<Set<Node>> output = new ArrayList<>();
        HashSet<Node> currentSociety = new HashSet<>();
        int i = 0;
        int returnIndex = Integer.MIN_VALUE;
        whileLoop: while(i < state.length || returnIndex >= 0) {
            if( state[i] != Integer.MIN_VALUE ) {
                int nextindeks = state[i];
                currentSociety.add(this.graph.getNodeById(i));
                state[i] = Integer.MIN_VALUE;
                if( returnIndex == Integer.MIN_VALUE ) {
                    returnIndex = i;
                }
                i = nextindeks;
            } else if (returnIndex >= 0 ){
                Node nodeById = this.graph.getNodeById(i);
                /// was already processed!
                if( currentSociety.contains(nodeById )  ) {
                    // cycle in current search! append to current society
                    // add society to return list
                    // return to returnIndex
                    output.add(currentSociety);
                    currentSociety = new HashSet<>();
                    i = returnIndex+1;
                    returnIndex = Integer.MIN_VALUE;
                    continue whileLoop;
                } else {
                    // we merge with earlier processed society
                    //search for society in output list
                    //merge societies
                    //return to returnIndex
                    for(Set<Node> nodeList: output) {
                        if( nodeList.contains(nodeById ) ) {
                            nodeList.addAll(currentSociety);
                            currentSociety = new HashSet<>();
                            i = returnIndex+1;
                            returnIndex = Integer.MIN_VALUE;
                            continue whileLoop;
                        }
                    }
                    throw new RuntimeException("Conversion algorithm failed: Cannot find socciety to merge to");
                }
            } else {
                i++;
            }
        }
        return output;
    }
}
