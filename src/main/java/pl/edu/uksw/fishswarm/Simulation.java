package pl.edu.uksw.fishswarm;

import pl.edu.uksw.fishswarm.graph.Graph;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 * Created by aniarudi on 02.02.16.
 */
public class Simulation {
    private final double[][] distances;
    private final NMI nmi;
    private int maxIterations;
    private Graph graph;
    private RybkaState2ListSet converter;
    private RandomFishGenerator generator;
    private double eps;
    private int populationSize;
    private double visual;
    private int step;
    private double delta;
    private int tryNr;
    private Random random;
    private List<Integer>[] npfi;
    private double[] history;
    private double[] maxHistory;
    private boolean verbose;
    private Modularity modularity;
    private int leapWindow;
    public String graphName;

    public Simulation(int maxIterations, Graph graph, double eps, int populationSize, double visual, int step, double delta, int tryNr, boolean verbose, int mod, String graphName) {
        this.maxIterations = maxIterations;
        this.graph = graph;
        this.eps = eps;
        this.populationSize = populationSize;
        this.visual = visual;
        this.step = step;
        this.delta = delta;
        this.tryNr = tryNr;
        this.verbose = verbose;
        this.leapWindow = maxIterations/25;
        this.graphName = graphName;

        this.random = new Random();
        this.generator  = new RandomFishGenerator(this.graph);
        this.converter = new RybkaState2ListSet(graph);
        this.random = new Random();
        this.distances = new double[populationSize][populationSize];
        this.nmi = new NMI(this.graph);

        this.npfi = new List[populationSize];
        for(int i=0;i< populationSize;i++) {
            npfi[i]= new ArrayList<>();
        }
        this.history= new double[maxIterations];
        this.maxHistory= new double[maxIterations];

        switch(mod){
            case 1:
                this.modularity = new Modularity1(graph);  
            case 3:
                this.modularity = new Modularity3(graph);
        }
    }

    public Rybka run () {
        int t = 0;
        System.out.println();
        List<Rybka> swarm = new ArrayList<>();
        Plikopisanie allResults = new Plikopisanie(graphName);
        int bestSolution, better;
        for( int i =0; i < populationSize ;i++) {
            swarm.add(generator.randomFish(graph));
        }
        Rybka theBest = null;
        bestSolution= 0;
        better = 0;
        do {
            if( this.verbose) System.out.println("step: "+t+" best solution: "+calculateModularity(swarm.get(bestSolution)));
            
                calculateDistances(swarm);
                //todo Oblicz npf i i zapisz sąsiedztwo xi --chyba już
            for( int i =0; i < populationSize ;i++) {

               
                double original = calculateModularity(swarm.get(i));

                if( followNeighbor(swarm, i, bestSolution) <= original ) {
                    ;
                    if( followSwarm(swarm, i, bestSolution) <=original ) {
                       //if(dontfollowSwarm(swarm, i, bestSolution)<=original){
                        prey(swarm, i, bestSolution);
                      // }
                    }
                }
            }
            better = bestSolution(swarm);
            this.history[t] = swarm.get(better).getModularity();
            if(calculateModularity(swarm.get(bestSolution)) < calculateModularity(swarm.get(better)) ) {
                bestSolution = better;
                theBest = swarm.get(bestSolution);
            }
            
            this.maxHistory[t] = swarm.get(bestSolution).getModularity();
            allResults.dopisz(this.maxHistory[t] + " indeks eybki:"+bestSolution);
            if( t > leapWindow && Math.abs(maxHistory[t] - maxHistory[t-leapWindow]) < eps ) {
                uskok(swarm, bestSolution);
            }
            t = t + 1;
        } while( t < this.maxIterations);

        allResults.zamknij();
        
        return swarm.get(bestSolution);
    }

    private void calculateDistances(List<Rybka> swarm) {
        for (int i=0; i< populationSize; i++) {
            for (int j=i+1; j< populationSize; j++) {
                distances[i][j] = 1-nmi.calculate(swarm.get(i), swarm.get(j));
                distances[j][i] = distances[i][j];
            }
        }

        for (int i=0; i< populationSize; i++) {
            npfi[i].clear();
            for (int j=0; j< populationSize; j++) {
                if( i!= j && distances[i][j] < visual ) {
                    npfi[i].add(j);
                }
            }
        }
    }

    private double prey(List<Rybka> swarm, int i, int best) {

        int nr = 0;
        Rybka rybka = swarm.get(i);
        Rybka next;
        int nexti = -1;
        while ( nr < tryNr && npfi[i].size()> 0) {
            //nexti = random.nextInt(npfi[i].size());
            //next = swarm.get(npfi[i].get(nexti));

            nexti = random.nextInt(swarm.size());
            next = swarm.get(nexti);
            
            //if ( calculateModularity(next) > rybka.getModularity() || calculateModularity(gotoRybka ) > rybka.getModularity()) {
            if ( calculateModularity(next) > rybka.getModularity() && distances[i][nexti] < visual) {

                Rybka gotoRybka = gotoRybka(rybka, next);
                swarm.set(i, gotoRybka);
                if( this.verbose) System.out.println("Rybka poluje "+i);
                return calculateModularity(gotoRybka);
            }

            nr++;
        }
        //if(i == best)
          //  return swarm.get(i).getModularity();
       
        /*if( best != i) {*/
         int indexFish = random.nextInt(swarm.size()-1);
        Rybka fish = swarm.get(indexFish);
        int nSteps = random.nextInt(step);
        for(int k = 0; k < nSteps; k++){
            int randNode = random.nextInt(graph.getVertexCount());
            int index = random.nextInt(graph.getAdjListOfVertex(randNode).size());
            int nodeOffset = graph.getAdjListOfVertex(randNode).get(index);
            fish.setState(randNode, nodeOffset);
                  
        }
                
        calculateModularity(fish);
        swarm.set(indexFish, fish);
         
            if( this.verbose) System.out.println("Rybka skacze " + i);
            return fish.getModularity();
        /*} else {
            if( this.verbose) System.out.println("Ta rybka ma zakaz skakania");
            return 0.;
        }*/
    }

    private double  followSwarm(List<Rybka> swarm, int i, int best) {

       // if(i == best)
         //   return swarm.get(i).getModularity();
        
        double avgDis = 0.;
        for( int j =0; j < populationSize ;j++) {
            avgDis += distances[i][j];
        }
        avgDis = avgDis/npfi[i].size();
        int xc = -1;
        double argmin = Double.MAX_VALUE;
        for( int j =0; j < populationSize ;j++) {
            
            if( distances[i][j] <= visual && i!= j && distances[i][j] >= avgDis && argmin > distances[i][j] ) {
                xc = j;
                argmin = distances[i][j];
            }
        }
        if( xc != -1 && swarm.get(i).getModularity() < swarm.get(xc).getModularity() && 1.*npfi[xc].size()/populationSize < delta ) {
            Rybka rybka = gotoRybka(swarm.get(i), swarm.get(xc));
            swarm.set(i, rybka);
            if( this.verbose) System.out.println("Rybka płynie za ławicą "+i);
            return calculateModularity(rybka);
        }
        return 0.;
    }
    
     private double  dontfollowSwarm(List<Rybka> swarm, int i, int best) {

        if(i == best)
           return swarm.get(i).getModularity();
         
        double avgDis = 0.;
        for( int j =0; j < populationSize ;j++) {
            avgDis += distances[i][j];
        }
        avgDis = avgDis/npfi[i].size();
        int xc = -1;
        double argmin = Double.MIN_VALUE;
        for( int j =0; j < populationSize ;j++) {
            
            if( distances[i][j] <= visual && i!= j && distances[i][j] >= avgDis && argmin < distances[i][j] ) {
                xc = j;
                argmin = distances[i][j];
            }
        }
        if( xc != -1 && swarm.get(i).getModularity() < swarm.get(xc).getModularity() && 1.*npfi[xc].size()/populationSize < delta ) {
            Rybka rybka = gotoRybka(swarm.get(i), swarm.get(xc));
            swarm.set(i, rybka);
            if( this.verbose) System.out.println("Rybka płynie za ławicą "+i);
            return calculateModularity(rybka);
        }
        return 0.;
    }


    private double followNeighbor(List<Rybka> swarm, int i, int best) {
        
 //       if(i == best)
  //          return swarm.get(i).getModularity();
        
        Rybka source = swarm.get(i);

        if(npfi[i].size() > 0){
        int maxNeighbor = npfi[i].get(0);
        for(int neighbor : npfi[i]){
            if(calculateModularity(swarm.get(neighbor)) > calculateModularity(swarm.get(maxNeighbor)))
                maxNeighbor = neighbor;
        }
        
        Rybka target = swarm.get(maxNeighbor);

            if ( /*distances[i][maxNeighbor] < visual &&*/ 1.*npfi[maxNeighbor].size()/populationSize < delta && calculateModularity(source) < calculateModularity(target) ) {
                Rybka rybka = gotoRybka(source, target);
                if( this.verbose) System.out.println("Rybka płynie za sąsiadem "+i);

                    swarm.set(i, rybka);
                    return calculateModularity(rybka);
            }

        }
        return 0.;
    }

    private Rybka gotoRybka(Rybka source, Rybka dest) {
        int[] ints = generator.randomSteps(step);
        int[] state = Arrays.copyOf(source.getState(),source.getState().length );
        for (int i : ints) {
            state[i] = dest.getState()[i];
        }
        return new Rybka(state, source.getFirstVertex());
    }

    private void uskok(List<Rybka> swarm, int best) {
         int indexFish;
        if( this.verbose) System.out.println("Uskok");
        
        //{
            indexFish = random.nextInt(swarm.size()-1);
        //}while(indexFish == best);
                
        Rybka fish = swarm.get(indexFish);
        int nSteps = random.nextInt(step);
        for(int i = 0; i < nSteps; i++){
            int randNode = random.nextInt(graph.getVertexCount());
            int index = random.nextInt(graph.getAdjListOfVertex(randNode).size());
            int nodeOffset = graph.getAdjListOfVertex(randNode).get(index);
            fish.setState(randNode, nodeOffset);
                  
        }
                
        calculateModularity(fish);
        swarm.set(indexFish, fish);
        /*
        int index = random.nextInt(swarm.size()-1);
        if( best ==  index ) {
            index=swarm.size()-1;
        }
        Rybka fish = generator.randomFish(graph);
        calculateModularity(fish);
        swarm.set(index, fish);*/
    }

    private int bestSolution(List<Rybka> swarm) {
        double max = Double.MIN_VALUE;
        int result = 0;
        for (Rybka rybka : swarm) {
            if( calculateModularity(rybka) > max) {
                max = calculateModularity(rybka);
                result = swarm.indexOf(rybka);
            }
        }
        return result;
    }
    private double calculateModularity(Rybka rybka) {
        if ( rybka.getModularity() != null) {
            return rybka.getModularity();
        } else {
            double modularity = this.modularity.calculate(converter.convert(rybka));
            rybka.setModularity(modularity);
            return modularity;
        }
    }

    public double[] getHistory() {
        return history;
    }

    public double[] getMaxHistory() {
        return maxHistory;
    }
}
