package pl.edu.uksw.fishswarm;

import pl.edu.uksw.fishswarm.graph.GmlParser;
import pl.edu.uksw.fishswarm.graph.Graph;

import javax.swing.*;
import java.util.concurrent.TimeUnit;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        //System.out.println("celegans_metabolic");
        /*Graph pseudograph = GmlParser.readFromFile("resources/celegans_metabolic.gml");
       Simulation simulation = new Simulation(100, pseudograph, 0.001, 100, 0.8, (int)(0.2*pseudograph.getVertexCount()), 0.5, 10, false,3  ,"Celegance");
        for(int i=1; i<=10; i++) {
            simulation.graphName = "Celegance"+i;
            Rybka rybka = simulation.run();
            System.out.println(rybka.getModularity());
        }*/
        
        
        System.out.println("dolphins");
        Graph pseudograph = GmlParser.readFromFile("resources/dolphins.gml");
        Simulation4 simulation = new Simulation4(100, pseudograph, 0.001,50, 0.8, (int)(0.2*pseudograph.getVertexCount()), 0.5, 10, false,3 , "Dolphins" );
        for(int i=1; i<=10; i++) {
            simulation.graphName = "Dolphins"+i;
            Rybka rybka = simulation.run();
            System.out.println(rybka.getModularity());
        }
        
        System.out.println("karate");
        pseudograph = GmlParser.readFromFile("resources/karate.gml");
        simulation = new Simulation4(100, pseudograph, 0.001, 50, 0.8, (int)(0.2*pseudograph.getVertexCount()), 0.5, 10, false ,3 , "Karate");
        for(int i=1; i<=10; i++) {
            simulation.graphName = "Karate"+i;
            Rybka rybka = simulation.run();
            System.out.println(rybka.getModularity());
        }
        System.out.println("footballTSEinput");
        pseudograph = GmlParser.readFromFile("resources/footballTSEinput.gml");
        simulation = new Simulation4(100, pseudograph, 0.001, 50, 0.8, (int)(0.2*pseudograph.getVertexCount()), 0.5, 10, false,3 , "Football" );
        for(int i=1; i<=10; i++) {
            simulation.graphName = "Football"+i;
            Rybka rybka = simulation.run();
            System.out.println(rybka.getModularity());
        }


    }
}
