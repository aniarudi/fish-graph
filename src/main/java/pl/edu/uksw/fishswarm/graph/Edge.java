package pl.edu.uksw.fishswarm.graph;

import org.jgrapht.graph.DefaultWeightedEdge;

import java.io.Serializable;

/**
 * Created by aniarudi on 03.02.16.
 */
public class Edge implements Cloneable, Serializable {
    private static final long serialVersionUID = -7200262325941711331L;
    private double weight;
    public Edge(double weight) {
        this.weight = weight;
    }
    public Edge() {
        this(1.0D);
    }
    public double getWeight() {
        return weight;
    }

    @Override
    public String toString() {
        return "Edge{" +
                "weight=" + weight +
                '}';
    }
}
