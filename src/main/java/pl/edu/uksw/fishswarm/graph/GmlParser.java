package pl.edu.uksw.fishswarm.graph;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by aniarudi on 03.02.16.
 */
public class GmlParser {
    private static Logger logger = Logger.getLogger(GmlParser.class.getName());

    public static Graph readFromFile(String filename) {
        File file = new File(filename);
        String word;
        String from = null;
        String to = null;
        int nodeCounter = 0;
        Scanner scanner = null;
        HashMap<String, Node> map = new HashMap<>();

        try {
            scanner = new Scanner(file);
        } catch (FileNotFoundException ex) {
            logger.log(Level.SEVERE, null, ex);
        }
        Graph pseudograph = new Graph();

        String id = null;
        while (scanner.hasNext()) {
            word = scanner.next();
            if (word.equals("id")) {
                id = scanner.next();
                Node n = new Node(nodeCounter, Integer.parseInt(id));
                map.put(id, n);
                pseudograph.addVertex(n);
                pseudograph.addToAdjList(n);
                nodeCounter++;
            }
            if (word.equals("label")) {
                String label = scanner.next();
                label = label.replace("\"", "");
                map.get(id).setLabel(label);
            }
            if (word.equals("source")) {
                from = scanner.next();
            }
            if (word.equals("target")) {
                to = scanner.next();
                Node f = map.get(from);
                Node t = map.get(to);

                Edge edge = new Edge();
                pseudograph.addEdge(f, t, edge);
                f.setStrength(f.getStrength()+edge.getWeight());
                t.setStrength(t.getStrength()+edge.getWeight());
                pseudograph.addVertexToAdjList(f, t);

            }

        }
        return pseudograph;
    }
}
