package pl.edu.uksw.fishswarm.graph;

import java.io.Serializable;
import java.util.List;

/**
 * Created by aniarudi on 03.02.16.
 */
public class Node implements Serializable {
    private static final long serialVersionUID = 7997852404251533000L;
    private String label;
    private int id;
    private int originalId;
    private double strength;
  

    public Node(int id, int originalId) {
        this.id = id;
        this.originalId = originalId;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

    public int getId() {
        return id;
    }

    public int getOriginalId() {
        return originalId;
    }

    public double getStrength() {
        return strength;
    }

    public void setStrength(double strength) {
        this.strength = strength;
    }

    @Override
    public String toString() {
        return "Node{" +
                "id=" + id +
                ", strength=" + strength +
                '}';
    }
}
