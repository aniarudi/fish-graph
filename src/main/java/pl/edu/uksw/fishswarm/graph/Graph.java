package pl.edu.uksw.fishswarm.graph;

import org.jgrapht.alg.ConnectivityInspector;
import org.jgrapht.graph.Pseudograph;

import java.util.*;
import java.util.List;

/**
 * Created by aniarudi on 03.02.16.
 */
public class Graph extends Pseudograph<Node, Edge> {
    private static final long serialVersionUID = 1875446431450154472L;

    private List<List<Integer> > adjacencyList;

    public Graph() {
        super(Edge.class);
        this.adjacencyList = new ArrayList<>();

    }
    

    public void addVertexToAdjList(Node n1, Node n2){
      
        if(!adjacencyList.get(n1.getId()).contains((n2.getId()))){
           this.adjacencyList.get(n1.getId()).add(n2.getId()); 
        }
        if(!adjacencyList.get(n2.getId()).contains((n1.getId()))){
           this.adjacencyList.get(n2.getId()).add(n1.getId()); 
        }
        
    }
    public void addToAdjList(Node n){
        this.adjacencyList.add(new ArrayList<Integer>());
    }
    
    public List<Integer> getAdjListOfVertex(int i){
        return this.adjacencyList.get(i);
    }
    public int getVertexCount() {
        return vertexSet().size();
    }
    public int getEdgeCount() {
        return edgeSet().size();
    }

    public Node getNodeById(int id) {
        Node n = null;
        for (Node node : vertexSet()) {
            if (node.getId() == id) {
                n = node;
                break;
            }
        }
        return n;
    }
    
    
}
