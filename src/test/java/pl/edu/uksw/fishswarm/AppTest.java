package pl.edu.uksw.fishswarm;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.jgrapht.VertexFactory;
import pl.edu.uksw.fishswarm.graph.Generator;
import pl.edu.uksw.fishswarm.graph.GmlParser;
import pl.edu.uksw.fishswarm.graph.Graph;
import pl.edu.uksw.fishswarm.graph.Node;

import java.util.*;

/**
 * Unit test for simple App.
 */
public class AppTest 
    extends TestCase
{
    private static final double EPSILON = 0.00000010;

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testRandomFishGenerator()
    {
        int length = 100;
        Graph pseudograph = new Graph();
        Generator generator = new Generator( 10, 20);
        generator.generateGraph(pseudograph, new VertexFactory<Node>() {
            private int id=0;
            @Override
            public Node createVertex() {
                return new Node(id, id++);
            }
        }, null);
        RandomFishGenerator randomFishGenerator = new RandomFishGenerator(length, 0);
        Rybka rybka = randomFishGenerator.randomFish(pseudograph);
        assertEquals(length, rybka.getState().length);
        Set<Integer> uSet = new HashSet<>();
        for (int i=0;i<rybka.getState().length;i++) {
            uSet.add(rybka.getState()[i]);
        }
        assertEquals(length, uSet.size());
        System.out.println(rybka.toString());
    }

    public void testRybkaState2ListSet() throws Exception {
        Graph pseudograph = new Graph();
        Generator generator = new Generator( 10, 20);
        generator.generateGraph(pseudograph, new VertexFactory<Node>() {
            private int id=0;
            @Override
            public Node createVertex() {
                return new Node(id, id++);
            }
        }, null);

        RybkaState2ListSet converter = new RybkaState2ListSet(pseudograph);
        int min = 0;
        int max = pseudograph.getVertexCount();
        Modularity1 modularity = new Modularity1(pseudograph);

        RandomFishGenerator randomFishGenerator = new RandomFishGenerator(max, min);

        double q = 0;
        Rybka rybka = null;
        //while ( q==0){
            rybka = randomFishGenerator.randomFish(pseudograph);

            List<Set<Node>> converted = converter.convert(rybka);

            q = modularity.calculate(converted);
        //}
        System.out.println(" "+q+" "+rybka.toString());
    }
    public void testToRybkaAndCalculateQ() throws Exception {
        Graph pseudograph = GmlParser.readFromFile("resources/test.gml");
        RybkaState2ListSet rybkaState2ListSet = new RybkaState2ListSet(pseudograph);
        ListSet2RybkaState convert2rybka = new ListSet2RybkaState(pseudograph);
        Modularity1 modularity = new Modularity1(pseudograph);

        Rybka rybka = new Rybka(new int[]{0, 1}, 0);
        System.out.println(rybka);
        List<Set<Node>> li = rybkaState2ListSet.convert(rybka);
        System.out.println(li);
        System.out.println("#communieties: "+li.size());
        for (Set<Node> nodes: li) {
            System.out.println("community: "+nodes.size()+" "+nodes.toString());
        }
        Rybka rybka1 = convert2rybka.convert(li);
        System.out.println(rybka1);
        List<Set<Node>> li2 = rybkaState2ListSet.convert(rybka1);
        System.out.println(li.size());
        System.out.println(li2.size());
        assertEquals("calculate after conversion not equals original", modularity.calculate(li), modularity.calculate(li));
        assertEquals(rybka, rybka1);
        assertEquals(li, li2);
        System.out.println("q after convertion to rybka and back: "+modularity.calculate(li));
        System.out.println("q: "+rybka.toString());
        System.out.println("q: "+rybka1.toString());

    }
    public void testToRybkaAndCalculateQ2() throws Exception {
        Graph pseudograph = GmlParser.readFromFile("resources/test2.gml");
        RybkaState2ListSet rybkaState2ListSet = new RybkaState2ListSet(pseudograph);
        ListSet2RybkaState convert2rybka = new ListSet2RybkaState(pseudograph);
        Modularity1 modularity = new Modularity1(pseudograph);

        Rybka rybka = new Rybka(new int[]{1, 0,3,2}, 0);
        System.out.println(rybka);
        List<Set<Node>> li = rybkaState2ListSet.convert(rybka);
        System.out.println(li);
        System.out.println("#communieties: "+li.size());
        for (Set<Node> nodes: li) {
            System.out.println("community: "+nodes.size()+" "+nodes.toString());
        }
        Rybka rybka1 = convert2rybka.convert(li);
        System.out.println(rybka1);
        List<Set<Node>> li2 = rybkaState2ListSet.convert(rybka1);
        System.out.println(li.size());
        System.out.println(li2.size());
        assertEquals("calculate after conversion not equals original", modularity.calculate(li), modularity.calculate(li));
        assertEquals(rybka, rybka1);
        assertEquals(li, li2);
        System.out.println("q after convertion to rybka and back: "+modularity.calculate(li));
        System.out.println("q: "+rybka.toString());
        System.out.println("q: "+rybka1.toString());

    }

    public void testModularity2() throws Exception {

        Graph pseudograph = GmlParser.readFromFile("resources/test.gml");
        RybkaState2ListSet rybkaState2ListSet = new RybkaState2ListSet(pseudograph);
        ListSet2RybkaState convert2rybka = new ListSet2RybkaState(pseudograph);
        Modularity2 modularity = new Modularity2(pseudograph);
        Rybka rybka = new Rybka(new int[]{0, 1}, 0);

        System.out.println(modularity.calculate(rybkaState2ListSet.convert(rybka)));
        assertEquals(-1.0, modularity.calculate(rybkaState2ListSet.convert(rybka)));

        rybka = new Rybka(new int[]{1, 0}, 0);
        System.out.println(modularity.calculate(rybkaState2ListSet.convert(rybka)));
    }
    public void testModularity2test2() throws Exception {

        Graph pseudograph = GmlParser.readFromFile("resources/test2.gml");
        RybkaState2ListSet rybkaState2ListSet = new RybkaState2ListSet(pseudograph);
        ListSet2RybkaState convert2rybka = new ListSet2RybkaState(pseudograph);
        Modularity2 modularity = new Modularity2(pseudograph);
        Rybka rybka = new Rybka(new int[]{0, 0,2, 2}, 0);

        System.out.println(modularity.calculate(rybkaState2ListSet.convert(rybka)));
        assertEquals(1.0, modularity.calculate(rybkaState2ListSet.convert(rybka)));

        rybka = new Rybka(new int[]{0, 1, 0, 1}, 0);
        assertEquals(-1.0, modularity.calculate(rybkaState2ListSet.convert(rybka)));
        System.out.println(modularity.calculate(rybkaState2ListSet.convert(rybka)));
    }

      public void testModularity2test3() throws Exception {

        Graph pseudograph = GmlParser.readFromFile("resources/test3.gml");
        RybkaState2ListSet rybkaState2ListSet = new RybkaState2ListSet(pseudograph);
        ListSet2RybkaState convert2rybka = new ListSet2RybkaState(pseudograph);
        Modularity2 modularity = new Modularity2(pseudograph);
        Rybka rybka = new Rybka(new int[]{0, 0,0, 0, 4, 4, 4, 4}, 0);

        System.out.println(modularity.calculate(rybkaState2ListSet.convert(rybka)));
        assertEquals(0.89, modularity.calculate(rybkaState2ListSet.convert(rybka)));

        rybka = new Rybka(new int[]{0, 1, 0, 1,0, 1, 0, 1}, 0);
        assertEquals(0.040000000000000036, modularity.calculate(rybkaState2ListSet.convert(rybka)));
        System.out.println(modularity.calculate(rybkaState2ListSet.convert(rybka)));
    }
      public void testModularity2test4() throws Exception {

        Graph pseudograph = GmlParser.readFromFile("resources/test4.gml");
        RybkaState2ListSet rybkaState2ListSet = new RybkaState2ListSet(pseudograph);
        ListSet2RybkaState convert2rybka = new ListSet2RybkaState(pseudograph);
        Modularity2 modularity = new Modularity2(pseudograph);
        Rybka rybka = new Rybka(new int[]{0, 0,0, 0, 4, 4, 4, 4}, 0);

        System.out.println(modularity.calculate(rybkaState2ListSet.convert(rybka)));
        assertEquals(1.0, modularity.calculate(rybkaState2ListSet.convert(rybka)));

        rybka = new Rybka(new int[]{0, 1, 0, 1,0, 1, 0, 1}, 0);
        assertEquals(-0.1111111111111111, modularity.calculate(rybkaState2ListSet.convert(rybka)));
        System.out.println(modularity.calculate(rybkaState2ListSet.convert(rybka)));
    }

    public void testModularity() throws Exception {
        Graph pseudograph = GmlParser.readFromFile("resources/test.gml");
        RybkaState2ListSet rybkaState2ListSet = new RybkaState2ListSet(pseudograph);
        ListSet2RybkaState convert2rybka = new ListSet2RybkaState(pseudograph);
        Modularity1 modularity = new Modularity1(pseudograph);
        Rybka rybka = new Rybka(new int[]{0, 1}, 0);

        assertEquals(-0.5, modularity.calculate(rybkaState2ListSet.convert(rybka)));

        rybka = new Rybka(new int[]{1, 0}, 0);
        System.out.println(modularity.calculate(rybkaState2ListSet.convert(rybka)));
    }

    public void testDirected() throws Exception {
        Graph pseudograph = GmlParser.readFromFile("resources/test.gml");
        Node node0 = pseudograph.getNodeById(0);
        Node node1 = pseudograph.getNodeById(1);
        assertTrue(pseudograph.containsEdge(node0, node1));
        assertTrue(pseudograph.containsEdge(node1, node0));

    }

    public void testNMI() throws Exception {
        Graph pseudograph = GmlParser.readFromFile("resources/karate.gml");

        Rybka rybka = new Rybka(new int[]{1, 2, 3, 4, 5, 6, 7, 9, 14, 10, 11, 12, 13, 16, 15, 18, 17, 19, 20, 21, 22, 0, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 8}, 0);
        Rybka rybka3 = new Rybka(new int[]{16, 17, 3, 21, 5, 6, 10, 13, 26, 27, 11, 12, 0, 1, 30, 32, 19, 2, 20, 4, 22, 7, 8, 24, 25, 9, 29, 28, 31, 14, 15, 23, 33, 18},0);
        int[] state = Arrays.copyOf(rybka.getState(), rybka.getState().length);
        state[0] = 0;
        state[1] = 1;
        Rybka rybka2 = new Rybka(state , rybka.getFirstVertex());

        NMI nmi = new NMI(pseudograph);
        double calculate = nmi.calculate(rybka, rybka2);
        System.out.println(nmi.getSwapList());
        System.out.println(calculate);
        assertTrue("NMI of modifide fishis cannot be equal", calculate<1.);
        assertEquals(calculate, nmi.calculate(rybka2, rybka));

        calculate = nmi.calculate(rybka, rybka);
        System.out.print(rybka);
        assertEquals(1., calculate);
        calculate = nmi.calculate(rybka3, rybka3);
        assertEquals(1., calculate);
        System.out.println(calculate);



    }
    public void testSimulation() throws Exception {
        Graph pseudograph = GmlParser.readFromFile("resources/karate.gml");
        Simulation simulation = new Simulation(10, pseudograph, 0.001, 50, 0.8, (int)(0.2*pseudograph.getVertexCount()), 0.5, 10, false ,3, "Karate" );
        Rybka rybka = simulation.run();
        System.out.println(rybka.getModularity());

    }
}

